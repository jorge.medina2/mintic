#!/bin/bash
docker tag wso2-mssql registry.gitlab.com/jorge.medina2/mintic/wso2-mssql
docker push registry.gitlab.com/jorge.medina2/mintic/wso2-mssql
docker tag wso2ei-integrator-mintic:6.6.0.$WSO2_PATCH.1-centos7 registry.gitlab.com/jorge.medina2/mintic/wso2ei-integrator-mintic:6.6.0.$WSO2_PATCH.1-centos7
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-integrator-mintic:6.6.0.$WSO2_PATCH.1-centos7
docker tag wso2ei-analytics-worker-mintic:6.6.0.$WSO2_PATCH.1-centos7 registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-worker-mintic:6.6.0.$WSO2_PATCH.1-centos7
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-worker-mintic:6.6.0.$WSO2_PATCH.1-centos7
docker tag wso2ei-an-dash-mintic:6.6.0.$WSO2_PATCH.1-centos7 registry.gitlab.com/jorge.medina2/mintic/wso2ei-an-dash-mintic:6.6.0.$WSO2_PATCH.1-centos7
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-an-dash-mintic:6.6.0.$WSO2_PATCH.1-centos7