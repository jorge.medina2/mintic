#!/bin/bash
docker tag wso2-mssql registry.gitlab.com/jorge.medina2/mintic/wso2-mssql
docker push registry.gitlab.com/jorge.medina2/mintic/wso2-mssql
docker tag wso2ei-integrator:6.6.0.$WSO2_PATCH.1 registry.gitlab.com/jorge.medina2/mintic/wso2ei-integrator:6.6.0.$WSO2_PATCH.1
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-integrator:6.6.0.$WSO2_PATCH.1
docker tag wso2ei-analytics-worker:6.6.0.$WSO2_PATCH.1 registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-worker:6.6.0.$WSO2_PATCH.1
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-worker:6.6.0.$WSO2_PATCH.1
docker tag wso2ei-analytics-dashboard:6.6.0.$WSO2_PATCH.1 registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-dashboard:6.6.0.$WSO2_PATCH.1
docker push registry.gitlab.com/jorge.medina2/mintic/wso2ei-analytics-dashboard:6.6.0.$WSO2_PATCH.1