# set base Docker image to latest CentOS Docker image
# FROM wso2ei:6.6.0-base-multi as builder
# LABEL maintainer="WSO2 Docker Mintic <dev@mintic.co>"

# set Docker image build arguments
# build arguments for user/group configurations

ARG  PATCH_CODE=undefined
FROM wso2ei-base:6.6.0.${PATCH_CODE}.1-centos7 as builder

ARG USER=wso2carbon
ARG USER_ID=802
ARG USER_GROUP=wso2
ARG USER_GROUP_ID=802
ARG USER_HOME=/home/${USER}

# build arguments for WSO2 product installation
ARG WSO2_SERVER_NAME=wso2ei
ARG WSO2_SERVER_VERSION=6.6.0
ARG WSO2_SERVER=${WSO2_SERVER_NAME}-${WSO2_SERVER_VERSION}
ARG WSO2_SERVER_HOME=${USER_HOME}/${WSO2_SERVER}

# build argument for MOTD (Message of the Day)
ARG MOTD="\n\
Welcome to WSO2 Docker resources.\n\
------------------------------------ \n\
This Docker container comprises of a WSO2 product, running with its latest GA release \n\
which is under the Apache License, Version 2.0. \n\
Read more about Apache License, Version 2.0 here @ http://www.apache.org/licenses/LICENSE-2.0.\n"


# create the non-root user and group and set MOTD login message
RUN \
    groupadd --system -g ${USER_GROUP_ID} ${USER_GROUP} \
    && useradd --system --create-home --home-dir ${USER_HOME} --no-log-init -g ${USER_GROUP_ID} -u ${USER_ID} ${USER} \
    && echo '[ ! -z "${TERM}" -a -r /etc/motd ] && cat /etc/motd' >> /etc/bash.bashrc; echo "${MOTD}" > /etc/motd

# install required packages
RUN \
    yum -y update \
    && yum install -y \
        zip \
        unzip \
    && rm -rf /var/cache/yum/*

# COPY --from=builder ${WSO2_SERVER_HOME} ${WSO2_SERVER_HOME}
# COPY --from=builder ${WSO2_SERVER_HOME} ${WSO2_SERVER_HOME}


# generate WSO2 EI profile
RUN \
	$WSO2_SERVER_HOME/bin/profile-creator.sh <<< 2 \
    && rm -R $WSO2_SERVER_HOME \
    && unzip -d ${USER_HOME} $USER_HOME/${WSO2_SERVER}_analytics.zip \
    && rm $USER_HOME/${WSO2_SERVER}_analytics.zip


FROM centos:7
LABEL maintainer="WSO2 Docker Mintic <dev@mintic.co>"

# set Docker image build arguments
# build arguments for user/group configurations
ARG USER=wso2carbon
ARG USER_ID=802
ARG USER_GROUP=wso2
ARG USER_GROUP_ID=802
ARG USER_HOME=/home/${USER}
# set JDK configurations
ARG JAVA_HOME=${USER_HOME}/java
ARG WSO2_JDK_URL=https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u242-b08/OpenJDK8U-jdk_x64_linux_hotspot_8u242b08.tar.gz
ARG WSO2_JDK_FILE=OpenJDK-jdk_x64_linux_hotspot.tar
ARG WSO2_JDK_CHECKSUM=f39b523c724d0e0047d238eb2bb17a9565a60574cf651206c867ee5fc000ab43
# build arguments for WSO2 product installation
ARG WSO2_SERVER_NAME=wso2ei
ARG WSO2_SERVER_VERSION=6.6.0
ARG WSO2_SERVER=${WSO2_SERVER_NAME}-${WSO2_SERVER_VERSION}
ARG WSO2_SERVER_HOME=${USER_HOME}/${WSO2_SERVER}

# build argument for MOTD (Message of the Day)
ARG MOTD="\n\
Welcome to WSO2 Docker resources.\n\
------------------------------------ \n\
This Docker container comprises of a WSO2 product, running with its latest GA release \n\
which is under the Apache License, Version 2.0. \n\
Read more about Apache License, Version 2.0 here @ http://www.apache.org/licenses/LICENSE-2.0.\n"

# create the non-root user and group and set MOTD login message
RUN \
    groupadd --system -g ${USER_GROUP_ID} ${USER_GROUP} \
    && useradd --system --create-home --home-dir ${USER_HOME} --no-log-init -g ${USER_GROUP_ID} -u ${USER_ID} ${USER} \
    && echo '[ ! -z "${TERM}" -a -r /etc/motd ] && cat /etc/motd' >> /etc/bash.bashrc; echo "${MOTD}" > /etc/motd

# copy wso2EI profile
COPY --from=builder --chown=wso2carbon:wso2 ${WSO2_SERVER_HOME} ${WSO2_SERVER_HOME}

# copy init script to user home
COPY --chown=wso2carbon:wso2 docker-entrypoint.sh ${USER_HOME}/

# install required packages
RUN \
    yum -y update \
    && yum install -y \
        nc \
        unzip \
        wget \
    && rm -rf /var/cache/yum/*

# install AdoptOpenJDK HotSpot
RUN \
    mkdir -p ${JAVA_HOME} \
    && wget -O ${WSO2_JDK_FILE} ${WSO2_JDK_URL} \
    && echo "${WSO2_JDK_CHECKSUM}  ${WSO2_JDK_FILE}" | sha256sum -c - \
    && tar -xf ${WSO2_JDK_FILE} -C ${JAVA_HOME} --strip-components=1 \
    && chown wso2carbon:wso2 -R ${JAVA_HOME} \
    && rm -f ${WSO2_JDK_FILE}

# set the user and work directory
USER ${USER_ID}
WORKDIR ${USER_HOME}

# set environment variables
ENV JAVA_HOME=${JAVA_HOME} \
    PATH=${JAVA_HOME}/bin:${PATH} \
    WORKING_DIRECTORY=${USER_HOME} \
    WSO2_SERVER_HOME=${WSO2_SERVER_HOME}

# expose server ports
EXPOSE 7712

# initiate container and start WSO2 Carbon server
ENTRYPOINT ["/home/wso2carbon/docker-entrypoint.sh"]

