#wait for the SQL Server to come up
sleep 90s

#run the setup script to create the DB and the schema in the DB
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Wso2carbon -d master -i setup_user.sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Wso2carbon -d wso2ei_usr -i mssql.sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Wso2carbon -d master -i setup_reg.sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Wso2carbon -d wso2ei_reg -i mssql.sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Wso2carbon -d wso2ei_reg -i setup_analytics.sql
