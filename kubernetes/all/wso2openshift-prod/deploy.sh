oc apply -f namespace-openshift.yaml
oc apply -f openshift-limit.yaml
oc apply -f configmap-wso2.yaml
oc apply -f wso2-secret-string-data.yaml
oc apply -f pv-pvc-carbon-apps.yaml